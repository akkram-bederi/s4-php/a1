<?php require_once './code.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S4: Access Modifiers and Inheritance</title>
</head>
<body>
	<h1>Building</h1>
	<p>The name of building is <?= $building->getName() ?></p>
	<p>The Caswynn Building has <?= $building->getFloors() ?> floors.</p>
	<p>The Caswynn Building is located at <?= $building->getAddress() ?></p>
	<?php $building->setName('Caswynn Complex') ?>
	<p>The name of the building has been changed to <?= $building->getName() ?>.</p>

	<h1>Condominium</h1>
	<p>The name of condominium is <?= $condominium->getName() ?></p>
	<p>The Enzo Condo has <?= $condominium->getFloors() ?> floors.</p>
	<p>The Enzo Condo is located at <?= $condominium->getAddress() ?></p>
	<?php $condominium->setName('Enzo Tower') ?>
	<p>The name of the condominium has been changed to <?= $condominium->getName() ?>.</p>
</body>
</html>